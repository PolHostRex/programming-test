# PROGRAMMING TEST

This is a programming test made with Unreal for HeroBeat Studios.

## CONTROLS

### Player 1
- [W][A][S][D] -> Movement
- [R] -> Spawn Bomb

### Player 2
- [I][J][K][L] -> Movement
- [P] -> Spawn Bomb

### Button
- [Space Bar] -> Click Restart Button

## GAMEPLAY

The map is inside the folder Content/Maps.
The player has to kill the other player with bomb. Until a bomb explodes, you won't be able to spawn more bombs, until
you pick a booster. Boosters will be explained later.
Light brown walls can be destroyed by the explosion of the bomb.
The bomb explosion can only go through players and boosters. If the bomb blast hits a breakable wall, the blast will stop.
The blast shows the area that can kill the player. If a the blast hits a player, they die and the other player wins.
The game lasts 90 seconds and this time is shown in the UI at the top center part of the screen. If the countdown reaches 0 
and the two players are alive, it's a draw.
Every time the game ends, it will appear a button in the bottom center part of the screen that will allow you to restart
the level.
There is a win counter in the right side of the screen. This counter will save the information through games.

### Boosters

There are two different boosters. Every booster lasts 6 seconds. 
The pink one is the speed booster that allows the player to increase it's velocity.
The red one allows the player to spawn bombs in a shorter interval of time. 
If the player tries to pick up a booster they already have, they won't pick it up.

## CREATOR

- Pol Recasens Sarrà

### External content

The player movement and the bomb blast are taken from a YouTube tutorial.

Movement:https://www.youtube.com/watch?v=zQhwxdJM3jA&t=285s

Bomb blast: https://www.youtube.com/watch?v=l7lJQyra3Bk&t=483sç

### Next Steps

The next steps will be to create the other two boosters: long bomb blast and remote bomb.