// Fill out your copyright notice in the Description page of Project Settings.


#include "UIClass.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
AUIClass::AUIClass()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AUIClass::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUIClass::Tick(float DeltaTime)
{
	//Set countdown timer
	Super::Tick(DeltaTime);
	float temp = timer;
	temp -= DeltaTime;

	if (temp <= 0.0f) {
		draw = true;
		UGameplayStatics::SetGamePaused(GetWorld(), true);
	}
	else {
		timer = temp;
	}

}

