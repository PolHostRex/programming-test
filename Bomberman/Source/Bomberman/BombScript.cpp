// Fill out your copyright notice in the Description page of Project Settings.


#include "BombScript.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>

// Sets default values
ABombScript::ABombScript()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABombScript::SpawnExplosion()
{
	// Spawn 4 bomb blast in the four main directions
	FVector Location = AActor::GetActorLocation();
	TArray<FVector> rotations = { {1.0f,0.0f,0.0f},{0.0f,1.0f,0.0f},{-1.0f,0.0f,0.0f},{0.0f,-1.0f,0.0f} }; 

	FActorSpawnParameters SpawnInfo;

	for (auto& vec : rotations) //for every rotation, spawn an explosion
	{
		GetWorld()->SpawnActor<AActor>(ExplosionClass, Location, UKismetMathLibrary::MakeRotFromX(vec), SpawnInfo);
	}

	//Destroy the bomb
	AActor::Destroy();

}

// Called when the game starts or when spawned
void ABombScript::BeginPlay()
{
	Super::BeginPlay();
	//Once the bomb is spawned, call the spawn explision after 3 seconds
	GetWorld()->GetTimerManager().SetTimer(FuzeTimerHandle, this, &ABombScript::SpawnExplosion, 3.0f, false);
}

// Called every frame
void ABombScript::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

