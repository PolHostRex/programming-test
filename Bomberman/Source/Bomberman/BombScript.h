// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BombScript.generated.h"

UCLASS()
class BOMBERMAN_API ABombScript : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABombScript();
	UFUNCTION(BlueprintImplementableEvent, Category = "Spawn")
		void onSpawn();
	UFUNCTION()
		void SpawnExplosion();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BPClasses)
		UClass* ExplosionClass;
	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FTimerHandle FuzeTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
