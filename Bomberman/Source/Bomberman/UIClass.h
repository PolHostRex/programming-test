// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UIClass.generated.h"

UCLASS()
class BOMBERMAN_API AUIClass : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUIClass();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float timer{ 90.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool draw{ false };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool player1Wins{ false };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool player2Wins{ false };
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
