// Fill out your copyright notice in the Description page of Project Settings.


#include "BoostManager.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetArrayLibrary.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABoostManager::ABoostManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABoostManager::GetRandomPosSpeedBoost()
{
	//This functions generates a random number to get a position to spawn. These positions are target points that are placed in the level
	//and are caught through an array of actors.
	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), targetPoint, "Speed", actors);
	
	bool different = false;
	int pos = -1;

	while (!different) 
	{
		//Here we make sure that the position where we want to spawn the boost is empty and no boosters where spawned there before
		//If the position is not valid, it will create another random number until you get one that is valid or every possible position is or was caught
		if (positionArraySpeed.size() >= actors.Num()) {
			different = true;
		}
		int number = UKismetMathLibrary::RandomIntegerInRange(0, actors.Num() - 1);

		if (std::find(positionArraySpeed.begin(), positionArraySpeed.end(), number) == positionArraySpeed.end())
		{
			different = true;
			positionArraySpeed.push_back(number);
			pos = number;
		}
	}
	if (pos != -1)
	{
		//If there's a valid position, spawn the booster in that position
		FActorSpawnParameters SpawnInfo;
		FTransform transform = actors[pos]->GetActorTransform();
		GetWorld()->SpawnActor<AActor>(SpeedClass,transform, SpawnInfo);
	}

}

void ABoostManager::GetRandomPosBombBoost()
{
	//This code has a similar functionality to the previous one with the difference that here we spawn the bomb booster
	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), targetPoint, "Bomb", actors);

	bool different = false;
	int pos = -1;

	while (!different)
	{
		if (positionArrayBomb.size() >= actors.Num()) {
			different = true;
		}
		int number = UKismetMathLibrary::RandomIntegerInRange(0, actors.Num() - 1);

		if (std::find(positionArrayBomb.begin(), positionArrayBomb.end(), number) == positionArrayBomb.end())
		{
			different = true;
			positionArrayBomb.push_back(number);
			pos = number;
		}
	}
	if (pos != -1)
	{
		FActorSpawnParameters SpawnInfo;
		FTransform transform = actors[pos]->GetActorTransform();
		GetWorld()->SpawnActor<AActor>(BombClass, transform, SpawnInfo);
	}
}

// Called when the game starts or when spawned
void ABoostManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABoostManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

