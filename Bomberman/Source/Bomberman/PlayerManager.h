// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerManager.generated.h"

UCLASS()
class BOMBERMAN_API APlayerManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerManager();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool ableSpawn{ true };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float timeBombSpawn{ 3.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float maxSpeed{ 200.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool speedBoost{ false };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool bombBoost{ false };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float force{ 5000.0f };
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BPClasses)
		UClass* BombClass;


	UFUNCTION(BlueprintCallable, Category = "Spawn")
		void SpawnBomb(FVector position, FRotator rotation);
	UFUNCTION(BlueprintCallable, Category = "SpeedBoost")
		void ManageSpeedBoostDuration();
	UFUNCTION(BlueprintCallable, Category = "BombBoost")
		void ManageBombBoostDuration();
	UFUNCTION(BlueprintCallable, Category = "SpeedBoostTimer")
		void SetSpeedBoostTimer(AActor* boostActor);
	UFUNCTION(BlueprintCallable, Category = "BombBoostTimer")
		void SetBombBoostTimer(AActor* boostActor);
	UFUNCTION(BlueprintCallable, Category = "ClampSpeed")
		void ClampSpeed(USceneComponent* cube, UPrimitiveComponent* cubePrim);
	UFUNCTION(BlueprintCallable, Category = "Horizontal")
		void MoveHorizontal(float value, USceneComponent* cube, UPrimitiveComponent* cubePrim);
	UFUNCTION(BlueprintCallable, Category = "Vertical")
		void MoveVertical(float value, USceneComponent* cube, UPrimitiveComponent* cubePrim);
	
	void SetAbleSpawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle FuzeTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
