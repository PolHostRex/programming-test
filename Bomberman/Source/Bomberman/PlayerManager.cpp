// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerManager.h"
#include "BombScript.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APlayerManager::APlayerManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void APlayerManager::SpawnBomb(FVector position, FRotator rotation)
{
	//If the player can place a bomb, spawn it in the actors position
	if (ableSpawn) 
	{
		FActorSpawnParameters SpawnInfo;
		GetWorld()->SpawnActor<ABombScript>(BombClass, position, rotation, SpawnInfo);
		ableSpawn = false;
		//Create a delay where the player is not able to spawn bombs. Once the delay time is completed, allow spawning bombs
		GetWorld()->GetTimerManager().SetTimer(FuzeTimerHandle, this, &APlayerManager::SetAbleSpawn, timeBombSpawn, false);
	}
}

void APlayerManager::ManageSpeedBoostDuration()
{
	//Once the speed boost is over, slow down
	speedBoost = false;
	maxSpeed -= 200.0f;
}

void APlayerManager::ManageBombBoostDuration()
{
	//Once the bomb boost is over, set the timer to spawn bomb to its original
	bombBoost = false;
	timeBombSpawn += 1.0f;
}

void APlayerManager::SetSpeedBoostTimer(AActor* boostActor)
{
	//Buff the players velocity
	boostActor->Destroy();
	maxSpeed += 200.0f;
	speedBoost = true;
}

void APlayerManager::SetBombBoostTimer(AActor* boostActor)
{
	//Minor time to spawn bombs
	boostActor->Destroy();
	timeBombSpawn -= 1.0f;
	bombBoost = true;
}

void APlayerManager::ClampSpeed(USceneComponent* cube, UPrimitiveComponent* cubePrim)
{
	// Clamp the velocity of the player if it exceeds the maximum allowed
	FVector velocity = cube->GetComponentVelocity();
	velocity.Z = 0.0f;

	if (velocity.Size() > maxSpeed)
	{
		FVector newVel = UKismetMathLibrary::Vector_ClampSize2D(velocity, 0.0f, maxSpeed);
		cubePrim->SetPhysicsLinearVelocity(newVel);
	}
}


void APlayerManager::SetAbleSpawn()
{
	ableSpawn = true;
}


// Called when the game starts or when spawned
void APlayerManager::BeginPlay()
{
	Super::BeginPlay();
}

void APlayerManager::MoveHorizontal(float value, USceneComponent* cube, UPrimitiveComponent* cubePrim)
{
	//Player input to move horizontally
	FVector forceToAdd = cube->GetRightVector()* (value * force);

	cubePrim->AddForce(forceToAdd, NAME_None, true);
	
}

void APlayerManager::MoveVertical(float value, USceneComponent* cube, UPrimitiveComponent* cubePrim)
{
	//Player input to move vertically
	FVector forceToAdd = cube->GetForwardVector() * (value * force);

	cubePrim->AddForce(forceToAdd, NAME_None, true);
}

// Called every frame
void APlayerManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

