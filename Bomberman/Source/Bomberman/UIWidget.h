// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UIClass.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UIWidget.generated.h"

/**
 *
 */
UCLASS()
class BOMBERMAN_API UUIWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* restartButton;
	UFUNCTION(BlueprintCallable, Category = "LoadGame")
		USaveGame* OnGetLoadGame(FString nameFile);
	UFUNCTION(BlueprintCallable, Category = "UIClass")
		AUIClass* GetUIClass();
	UFUNCTION(BlueprintCallable, Category = "Win")
		FString GetWhichPlayerWins(bool player1Win, bool player2Win);
	UFUNCTION(BlueprintCallable, Category = "Draw")
		FString GetDrawResult(bool draw);
	UFUNCTION()
		void OnRestartGame();

	void RestartGame();
	void NativeConstruct() override;
	void SetRestartButtonVisible();
	

};
