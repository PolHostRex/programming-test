// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UIClass.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplosionManager.generated.h"

UCLASS()
class BOMBERMAN_API AExplosionManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplosionManager();

	UFUNCTION(BlueprintCallable, Category = "UIClass")
		AUIClass* GetUIClass();
	UFUNCTION(BlueprintCallable, Category = "GetSaveGame")
		USaveGame* GetSaveGame(FString nameFile, TSubclassOf<USaveGame> saveGameClass);
	UFUNCTION(BlueprintCallable, Category = "SetSaveGame")
		void SetSaveGame(FString nameFile, UPARAM(Ref) int32& value, USaveGame* saveGame);


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		bool stop{ false };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float speed{ 500.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		float length{ 400.0f };
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Variables)
		FVector startPosition{ FVector::ZeroVector};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void BombBlastMove(float deltaTime);
	void DestroySelfActor();
	FTimerHandle FuzeTimerHandle;

};
