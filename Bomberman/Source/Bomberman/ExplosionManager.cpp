// Fill out your copyright notice in the Description page of Project Settings.


#include "ExplosionManager.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
AExplosionManager::AExplosionManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

AUIClass* AExplosionManager::GetUIClass()
{
	//return UI class
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUIClass::StaticClass(), FoundActors);
	return Cast<AUIClass>(FoundActors[0]);
}

USaveGame* AExplosionManager::GetSaveGame(FString nameFile, TSubclassOf<USaveGame> saveGameClass)
{
	//if save game file exist, return it, if not, create a new save game file
	bool existSlot = UGameplayStatics::DoesSaveGameExist(nameFile, 0);
	if (existSlot) {

		USaveGame* savegame = UGameplayStatics::LoadGameFromSlot(nameFile, 0);
		if (savegame)
			return savegame;
		else return nullptr;
	}
	else 
	{
		return UGameplayStatics::CreateSaveGameObject(saveGameClass);
	}
}

void AExplosionManager::SetSaveGame(FString nameFile, UPARAM(Ref) int32& value, USaveGame* saveGame)
{
	//Save game values to a specific save game file
	value++;
	UGameplayStatics::SaveGameToSlot(saveGame, nameFile, 0);
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

void AExplosionManager::BombBlastMove(float deltaTime)
{
	//Move the blast of the bomb and destroy it if it exceeds the maximum length
	if (!stop) 
	{
		float speedDelta = deltaTime * speed;
		FVector moveVector = AActor::GetActorForwardVector() * speedDelta;
		AActor::AddActorWorldOffset(moveVector, true);

		FVector distance = AActor::GetActorLocation() - startPosition;

		if (distance.Size() >= length) 
		{
			stop = true;
			//delay the destroy of the blast to have better visual feedback
			GetWorld()->GetTimerManager().SetTimer(FuzeTimerHandle, this, &AExplosionManager::DestroySelfActor, 0.5f, false); 
		}


	}
}

void AExplosionManager::DestroySelfActor()
{
	this->Destroy();
}


// Called when the game starts or when spawned
void AExplosionManager::BeginPlay()
{
	//Returns true if the blast hits a static world object and set the length to the distance hit because we want to destroy the blast
	startPosition = AActor::GetActorLocation();
	FVector endPosition = (AActor::GetActorForwardVector() * length) + startPosition;
	const TArray<TEnumAsByte<EObjectTypeQuery>> staticWorld = {EObjectTypeQuery::ObjectTypeQuery1};
	const TArray < AActor* > ActorsToIgnore;
	FHitResult hit;

	bool hitted = UKismetSystemLibrary::BoxTraceSingleForObjects(GetWorld(), startPosition, endPosition, FVector(35.0f, 35.0f, 35.0f), FRotator::ZeroRotator,
		staticWorld, false, ActorsToIgnore, EDrawDebugTrace::None, hit, true);
	
	if(hitted)
	{
		length = hit.Distance;
	}
	
	Super::BeginPlay();
	
}

// Called every frame
void AExplosionManager::Tick(float DeltaTime)
{
	BombBlastMove(DeltaTime);
	Super::Tick(DeltaTime);

}

