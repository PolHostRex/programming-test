// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <vector>
#include "BoostManager.generated.h"

UCLASS()
class BOMBERMAN_API ABoostManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoostManager();

	UFUNCTION(BlueprintCallable, Category = "SpeedBoost")
		void GetRandomPosSpeedBoost();
	UFUNCTION(BlueprintCallable, Category = "BombBoost")
		void GetRandomPosBombBoost();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BPClasses)
		TSubclassOf<AActor> targetPoint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BPClasses)
		UClass* SpeedClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BPClasses)
		UClass* BombClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	FTimerHandle FuzeTimerHandle;
	int lastIndexSpeed = -1;
	int lastIndexBomb = -1;
	std::vector<int> positionArraySpeed;
	std::vector<int> positionArrayBomb;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
