// Fill out your copyright notice in the Description page of Project Settings.


#include "UIWidget.h"
#include <Components/Button.h>
#include <Components/TextBlock.h>
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>


void UUIWidget::NativeConstruct()
{
	Super::NativeConstruct();

	restartButton->OnClicked.AddUniqueDynamic(this, &UUIWidget::OnRestartGame); //create a callback for the restart button
}

void UUIWidget::RestartGame()
{
	UGameplayStatics::OpenLevel(GetWorld(), "BombermanMap"); //Open the same level to restart it

}
USaveGame* UUIWidget::OnGetLoadGame(FString nameFile)
{
	//Return the desired save game file. If it doesn't exist, return null
	bool existSlot1 = UGameplayStatics::DoesSaveGameExist(nameFile, 0);
	if (existSlot1) {

		USaveGame* savegame =  UGameplayStatics::LoadGameFromSlot(nameFile, 0);
		if (savegame)
			return savegame;
		else return nullptr;
	}
	else
		return nullptr;
}

AUIClass* UUIWidget::GetUIClass()
{
	//Function that returns the UIClass
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUIClass::StaticClass(), FoundActors);
	return Cast<AUIClass>(FoundActors[0]);
}

FString UUIWidget::GetWhichPlayerWins(bool player1Win, bool player2Win)
{
	//Check which player wins to show it in the screen
	if (player1Win) {
		SetRestartButtonVisible();
		return "Player 1 Wins!";
	}
	else if (player2Win) {
		SetRestartButtonVisible();
		return "Player 2 Wins!";
	}
	else return "";
}

FString UUIWidget::GetDrawResult(bool draw)
{
	//If it's a draw, show it in the screen
	if (draw) {
		SetRestartButtonVisible();
		return "Draw";
	}
	return "";
}

void UUIWidget::SetRestartButtonVisible()
{
	//When the game ends, enable restart button
	restartButton->SetVisibility(ESlateVisibility::Visible);
	restartButton->SetIsEnabled(true);
	restartButton->SetFocus();
}

void UUIWidget::OnRestartGame()
{
	RestartGame();
}

